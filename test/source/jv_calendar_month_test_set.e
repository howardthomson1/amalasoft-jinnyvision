note
	description: "Summary description for {JV_CALENDAR_MONTH_TEST_SET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	JV_CALENDAR_MONTH_TEST_SET

inherit
	TEST_SET_SUPPORT

feature

	test_month_calendar
		local
			l_dialog: EV_INFORMATION_DIALOG
			l_calendar: JV_CALENDAR_MONTH
			l_app: EV_APPLICATION
			l_window: EV_TITLED_WINDOW
		do
			create l_dialog
			create l_calendar.make_with_date_field (create {JV_DATE_FIELD}, agent capture_date, agent no_capture_date)
			l_dialog.extend (l_calendar)


			create l_window.make_with_title ("test calendar")
			l_window.set_size (800, 600)
			
			create l_app

			l_app.post_launch_actions.extend (agent l_window.show)
			l_window.show_actions.extend (agent l_dialog.show_modal_to_window (l_window))
			l_window.close_request_actions.extend (agent l_app.destroy)
			l_dialog.close_request_actions.extend (agent l_app.destroy)

			l_app.launch
		end

feature {NONE}

	capture_date (a_date: DATE)
		do

		end

	no_capture_date
		do

		end

end
